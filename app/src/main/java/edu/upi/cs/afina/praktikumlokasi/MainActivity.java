package edu.upi.cs.afina.praktikumlokasi;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationListener;


public class MainActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private static final int MY_PERMISSION_REQUEST = 99;
    GoogleApiClient mGoogleApiClient​;
    Location mLastLocation​;
    TextView mLatText​;
    TextView mLongText​;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mLatText​ = (TextView)findViewById(R.id.tvLat);
        mLongText​ = (TextView)findViewById(R.id.tvLong);
        buildGoogleApiClient();
        createLocationRequest();


    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == MY_PERMISSION_REQUEST){
            if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                ambilLokasi();
            }else{
                AlertDialog ad = new AlertDialog.Builder(this).create();
                ad.setMessage("Tidak mendapat ijin, tidak dapat mengambil lokasi");
                ad.show();
            }
            return;
        }
    }

    protected synchronized void buildGoogleApiClient(){
        mGoogleApiClient​ = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    public void ambilLokasi(){
        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSION_REQUEST);
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient​, mLocationRequest, this);

//        if(mLastLocation​ != null){
//            mLatText​.setText("Latitude: " +String.valueOf(mLastLocation​.getLatitude()));
//            mLongText​.setText("Longitude: " +String.valueOf(mLastLocation​.getLongitude()));
//        }
    }

    LocationRequest mLocationRequest;
    protected void createLocationRequest(){
        mLocationRequest = new LocationRequest();
//        10 detik sekali meminta lokasi (10000ms = 10 detik)
        mLocationRequest.setInterval(10000);
//        tapi tidak boleh lebih cepat dari 5 detik
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }



    @Override
    protected void onStart(){
        super.onStart();
        mGoogleApiClient​.connect();
    }

    @Override
    protected void onStop(){
        mGoogleApiClient​.disconnect();
        super.onStop();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        ambilLokasi();

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    @Override
    public void onLocationChanged(Location location) {
        AlertDialog ad = new AlertDialog.Builder(this).create();
        ad.setMessage("Update lokasi");
        ad.show();

        mLatText​.setText("Latitude: " + String.valueOf(location.getLatitude()));
        mLongText​.setText("Longitude: " + String.valueOf(location.getLongitude()));

    }
}

